import React from "react";
import MainPage from "./Components/MainPage";
import { BrowserRouter, Route, Routes } from "react-router-dom";

import "./App.css";
import { QueryClient, QueryClientProvider } from "react-query";
import Home from "./Pages/HomePage";
import Navbar from "./Components/NavBar";

import Error from "./Pages/ErrorPage";
import Loading from "./Pages/LoadingPage";
import FormInstitutionPut from "./Components/PutInstitutionComponent";
import FormInstructorPut from "./Components/PutInstructorComponent";
import FormTrainingPut from "./Components/PutTrainingComponent";
import Institutions from "./Pages/InstitutionsPage";
import Instructors from "./Pages/InstructorsPage";
import Trainings from "./Pages/TrainingsPage";
import LoginPage from "Pages/LoginPage";
import { ProtectedRoute } from "CustomComponents/ProtectedRoute";

function App() {
  const queryClient = new QueryClient();
  return (
    <QueryClientProvider client={queryClient}>
      <BrowserRouter>
        <Navbar />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/login" element={<LoginPage />} />

          <Route
            path="/admin/"
            element={
              <ProtectedRoute>
                <MainPage />
              </ProtectedRoute>
            }
          >
            <Route path="/admin/etablissements" element={<Institutions />} />
            <Route path="/admin/formateurs" element={<Instructors />} />
            <Route path="/admin/formations" element={<Trainings />} />
            <Route path="loading" element={<Loading />} />
            <Route path="error" element={<Error />} />
            <Route
              path="/admin/etablissements/:id"
              element={<FormInstitutionPut />}
            />
            <Route
              path="/admin/formateurs/:id"
              element={<FormInstructorPut />}
            />
            <Route path="/admin/formations/:id" element={<FormTrainingPut />} />
          </Route>
        </Routes>
      </BrowserRouter>
    </QueryClientProvider>
  );
}

export default App;
