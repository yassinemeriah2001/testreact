import React, { useEffect, useState } from "react";
import { useMutation, useQuery, useQueryClient } from "react-query";
import Loading from "../Pages/LoadingPage";
import Error from "../Pages/ErrorPage";
import { IoIosAddCircleOutline } from "react-icons/io";
import {
  type ApiResponse,
  type Del,
  type Institution,
  UriEtab,
} from "../Services/Interfaces_Uris_Token";
import FormInstitution from "../Components/PostInstitutionComponent";
import InstitutionTable from "../CustomComponents/CustomInstitutionTable";
import { deleteData, getData } from "../Services/Functions";
import Title from "CustomComponents/CustomTitle";

const InstitutionData = () => {
  const [isAddOpen, setIsAddOpen] = useState(false);

  const toggleAddForm = () => {
    setIsAddOpen(!isAddOpen);
  };
  const useDelete = () => {
    const queryClient = useQueryClient();
    return useMutation({
      mutationFn: async ({ id, uri }: Del) => await deleteData(id, uri),
      onSuccess: () => {
        queryClient.invalidateQueries({ queryKey: ["Institution"] });
      },
    });
  };
  const { mutate: mdelete } = useDelete();

  const { data, status } = useQuery<ApiResponse<Institution[]>>(
    "Institution",
    async () => await getData(UriEtab),
  );
  const [Etablissements, setEtablissements] = useState<Institution[]>([]);
  const handleDelete = async (id: number) => {
    mdelete({ id, uri: UriEtab });
  };
  useEffect(() => {
    if (data?.data != null) {
      setEtablissements(data?.data);
    }
  }, [data?.data]);
  if (status === "loading") {
    return <Loading />;
  }
  if (status === "error") {
    return <Error />;
  }
  return (
    <div className="data-div">
      <Title
        title={"Etablissements"}
        className="title-data"
        undertitle={"TABLE"}
      />
      <div className="divdata1">
        <div className="add-container">
          <button onClick={toggleAddForm} className="data-add">
            <IoIosAddCircleOutline />
          </button>
        </div>
        <div className=" table-div">
          <InstitutionTable
            arrayInstitution={Etablissements}
            onDelete={handleDelete}
          />
        </div>
      </div>
      {isAddOpen && <FormInstitution handleclose={toggleAddForm} />}
    </div>
  );
};

export default InstitutionData;
