import React, { useEffect, useState } from "react";
import { useMutation, useQuery, useQueryClient } from "react-query";
import Loading from "../Pages/LoadingPage";
import Error from "../Pages/ErrorPage";
import { IoIosAddCircleOutline } from "react-icons/io";
import {
  type Del,
  UriForm,
  type ApiResponse,
  type Training,
} from "../Services/Interfaces_Uris_Token";
import { deleteData, getData } from "../Services/Functions";
import FormTraining from "../Components/PostTrainingComponent";
import TrainingTable from "../CustomComponents/CustomTrainingTable";
import Title from "CustomComponents/CustomTitle";

const TrainingData = () => {
  const [isAddOpen, setIsAddOpen] = useState(false);

  const toggleAddForm = () => {
    setIsAddOpen(!isAddOpen);
  };

  const useDelete = () => {
    const queryClient = useQueryClient();
    return useMutation({
      mutationFn: async ({ id, uri }: Del) => await deleteData(id, uri),
      onSuccess: () => {
        queryClient.invalidateQueries({ queryKey: ["training"] });
      },
    });
  };
  const { mutate: mdelete } = useDelete();

  const { data, status } = useQuery<ApiResponse<Training[]>>(
    "training",
    async () => await getData(UriForm),
  );
  const [Formations, setFormations] = useState<Training[]>([]);
  const handleDelete = async (id: number) => {
    mdelete({ id, uri: UriForm });
  };
  useEffect(() => {
    if (data?.data != null) {
      setFormations(data?.data);
    }
  }, [data?.data]);

  if (status === "loading") {
    return <Loading />;
  }
  if (status === "error") {
    return <Error />;
  }
  return (
    <div className="data-div">
      <Title title={"Formations"} className="title-data" undertitle={"TABLE"} />
      <div className="divdata1 ">
        <div className="add-container">
          <button onClick={toggleAddForm} className="data-add">
            <IoIosAddCircleOutline />
          </button>
        </div>
        <div className=" table-div">
          <TrainingTable arrayTraining={Formations} onDelete={handleDelete} />
        </div>
      </div>
      {isAddOpen && <FormTraining handleclose={toggleAddForm} />}
    </div>
  );
};

export default TrainingData;
