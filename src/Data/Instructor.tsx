import React, { useEffect, useState } from "react";
import { useMutation, useQuery, useQueryClient } from "react-query";
import Loading from "../Pages/LoadingPage";
import Error from "../Pages/ErrorPage";
import { IoIosAddCircleOutline } from "react-icons/io";
import {
  type Del,
  UriFormtr,
  type ApiResponse,
  type Instructor,
} from "../Services/Interfaces_Uris_Token";
import { deleteData, getData } from "../Services/Functions";
import FormInstructor from "../Components/PostInstructorComponent";
import InstructorTable from "../CustomComponents/CustomInstructorTable";
import Title from "CustomComponents/CustomTitle";

const InstructorData = () => {
  const [isAddOpen, setIsAddOpen] = useState(false);

  const toggleAddForm = () => {
    setIsAddOpen(!isAddOpen);
    refetch();
  };
  const useDelete = () => {
    const queryClient = useQueryClient();
    return useMutation({
      mutationFn: async ({ id, uri }: Del) => await deleteData(id, uri),
      onSuccess: () => {
        queryClient.invalidateQueries({ queryKey: ["Instructor"] });
      },
    });
  };
  const { mutate: mdelete } = useDelete();

  const { data, status, refetch } = useQuery<ApiResponse<Instructor[]>>(
    "Instructor",
    async () => await getData(UriFormtr),
  );
  const [Formateurs, setFormateurs] = useState<Instructor[]>([]);
  const handleDelete = async (id: number) => {
    mdelete({ id, uri: UriFormtr });
  };
  useEffect(() => {
    if (data?.data != null) {
      setFormateurs(data?.data);
    }
  }, [data?.data]);
  if (status === "loading") {
    return <Loading />;
  }
  if (status === "error") {
    return <Error />;
  }
  return (
    <div className="data-div">
      <Title title={"Formateurs"} className="title-data" undertitle={"TABLE"} />
      <div className="divdata1  ">
        <div className="add-container">
          <button onClick={toggleAddForm} className="data-add">
            <IoIosAddCircleOutline />
          </button>
        </div>
        <div className=" table-div">
          <InstructorTable
            arrayInstructor={Formateurs}
            onDelete={handleDelete}
          />
        </div>
      </div>
      {isAddOpen && <FormInstructor handleclose={toggleAddForm} />}
    </div>
  );
};

export default InstructorData;
