import React from "react";
import { MdError } from "react-icons/md";

const error = () => {
  return (
    <div>
      <h1>
        <MdError /> error 404
      </h1>
    </div>
  );
};

export default error;
