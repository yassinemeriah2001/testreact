import React from "react";
import { AiOutlineLoading3Quarters } from "react-icons/ai";

const loading = () => {
  return (
    <div>
      <h1>
        {" "}
        <AiOutlineLoading3Quarters />
        loading
      </h1>
    </div>
  );
};

export default loading;
