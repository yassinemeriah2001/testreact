import Title from "CustomComponents/CustomTitle";
import React from "react";
import { useMutation } from "react-query";

import { addData } from "Services/Functions";
import { Add, Evaluation, UriEval } from "Services/Interfaces_Uris_Token";
import Rate from "../CustomComponents/CustomEvaluationForm";

const Home = () => {
  const useAdd = () => {
    return useMutation({
      mutationFn: async ({ uri, data }: Add) => await addData(uri, data),
    });
  };
  const { mutate: add } = useAdd();
  const onSubmit = (values: Evaluation) => {
    add({
      uri: UriEval,
      data: JSON.stringify({ data: values }),
    });
  };

  return (
    <div className=" place-content-center flex">
      <div className=" div-form border-black border bg-gray-400 ">
        <Title
          title={"Fiche de"}
          className="title-form"
          undertitle={"Satisfaction"}
        />

        <div className="form-div ">
          <Rate add={onSubmit} />
        </div>
      </div>
    </div>
  );
};

export default Home;
