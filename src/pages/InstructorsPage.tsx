import React from "react";
import InstructorData from "../Data/Instructor";

const Instructors = () => {
  return <InstructorData />;
};

export default Instructors;
