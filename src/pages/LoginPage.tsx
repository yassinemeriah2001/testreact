import React from "react";
import { useAuth } from "Context/AuthContext";
import { logIn } from "Services/Functions";
import { Login } from "Services/Interfaces_Uris_Token";
import Loginform from "../CustomComponents/CustomLoginForm";
import { useNavigate } from "react-router-dom";

const LoginPage = () => {
  const { access, onSetAccess, onSetAuthenticated } = useAuth();
  const navigate = useNavigate();
  const onSubmit = async (values: Login) => {
    const res = await logIn(JSON.stringify(values));
    const accessToken = res.toString();
    onSetAccess(accessToken);
    if (access != null) {
      onSetAuthenticated(true);
      navigate("/admin/formateurs");
    }
  };
  return (
    <div className=" justify-center flex my-8 h-auto w-auto">
      <Loginform connect={onSubmit} />
    </div>
  );
};

export default LoginPage;
