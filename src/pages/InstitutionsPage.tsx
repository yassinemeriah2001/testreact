import React from "react";
import InstitutionData from "../Data/Institution";

const Institutions = () => {
  return <InstitutionData />;
};

export default Institutions;
