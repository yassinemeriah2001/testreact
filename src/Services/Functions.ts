import { ApiResponse, Login, Token } from "./Interfaces_Uris_Token";

/*-------fetchwrapper-------*/
const fetchWrapper = async <T>(
  path: string,
  config?: RequestInit,
): Promise<T> => {
  const init = config
    ? {
        ...config,
        headers: {
          ...config.headers,
          Authorization: Token,
          "Content-Type": "application/json",
        },
      }
    : {
        headers: {
          Authorization: Token,
          "Content-Type": "application/json",
        },
      };

  const request = new Request(path, init);
  const response = await fetch(request);

  if (!response.ok) {
    throw new Error(
      `Status Code : ${response.status}, message : ${response.statusText}`,
    );
  }

  return await response.json().catch(() => ({}));
};

/*-------Delete-------*/
const deleteData = async (id: number, Uri: string) => {
  const res = await fetchWrapper<any>(Uri + "/" + id, {
    method: "delete",
  });
  return res;
};

/*-------Get-------*/
const getData = async (Uri: string) => {
  const res = await fetchWrapper<any>(Uri, {
    method: "get",
  });
  return res;
};

/*-------GetById-------*/
const getDataById = async (Uri: string, id: number) => {
  const res = await fetchWrapper<any>(Uri + "/" + id, {
    method: "get",
  });
  return await res;
};

/*-------Post-------*/
const addData = async (Uri: string, data: string) => {
  const res = fetchWrapper<any>(Uri, {
    body: data,
    method: "POST",
  });
  return await res;
};

/*-------Put-------*/
const updateData = async (Uri: string, data: string, id: number) => {
  const res = fetchWrapper<any>(Uri + "/" + id, {
    body: data,
    method: "PUT",
  });
  return await res;
};

/*-------Logout-------*/
const logOut = () => {
  localStorage.removeItem("auth");
  if (localStorage.getItem("auth") == null) {
  }
};

/*-------Login-------*/
const logIn = async (data: string) => {
  let jwt = "";
  const res = fetchWrapper<ApiResponse<Login>>(
    "http://localhost:1337/api/auth/local",
    {
      body: data,
      method: "POST",
    },
  ).then((res) => {
    localStorage.setItem("auth", res.data.jwt);
    jwt += res.data.jwt;
  });
  return jwt;
};

export { deleteData, getData, addData, updateData, getDataById, logIn, logOut };
