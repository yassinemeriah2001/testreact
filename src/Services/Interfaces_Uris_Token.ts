interface ApiResponse<T> {
  data: T;
}
interface Institution {
  id: number;
  attributes: {
    nom: string;
    createdAt: string;
    updatedAt: string;
  };
}

interface Instructor {
  id: number;
  attributes: {
    Nom: string;
    prenom: string;
    sexe: string;
    Tel: number;
    email: string;
    createdAt: string;
    updatedAt: string;
  };
}
interface Training {
  id: number;
  attributes: {
    Theme: string;
    createdAt: string;
    updatedAt: string;
  };
}
interface Del {
  id: number;
  uri: string;
}
interface Add {
  uri: string;
  data: string;
}
interface Update {
  uri: string;
  data: string;
  id: number;
}
interface Login {
  identifier: string;
  password: string;
  jwt: string;
}

interface ButtonProp {
  handleclose: () => void;
}
interface Evaluation {
  Theme: string;
  Date: Date;
  Formateur: string;
  Nom: string;
  Prenom: string;
  Telephone: number;
  Email: string;
  Etablissement: string;
  Eval: string;
}
type ReqBody =
  | Evaluation[]
  | Training[]
  | Institution[]
  | Instructor[]
  | Login[];

const UriEtab = "http://localhost:1337/api/etablissements";
const UriFormtr = "http://localhost:1337/api/formateurs";
const UriForm = "http://localhost:1337/api/formations";
const UriEval = "http://localhost:1337/api/eval-forms";

const Token =
  "bearer c4d733e0026c5c855a138566b99317cebb6a9b51dcd12b5ed185941c398481d3f1570ec545198fa995b6c5bdda1fde7701b0d9847badc2f8f9c1dc28c852ef1352efe592745337684204d2ac42cb20f72d5109d9532641fc7b247cd38921097d52d325a6e74f82353bd34348420a77a1f509c4d4a1623fc360b3154fa25287c3";

export { UriEtab, UriForm, UriFormtr, Token, UriEval };

export type {
  ApiResponse,
  Institution,
  Instructor,
  Training,
  ButtonProp,
  Del,
  Add,
  Update,
  Evaluation,
  Login,
  ReqBody,
};
