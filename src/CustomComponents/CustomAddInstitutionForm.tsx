import { type ComponentProps, type FC } from "react";
import { useForm } from "react-hook-form";
import { type Institution } from "../Services/Interfaces_Uris_Token";
import Button from "./CustomButton";

export type Form = ComponentProps<"form"> & {
  onFormSubmit: (values: Institution) => any;
  onCancel: () => void;
};

const FormAddInstitution: FC<Form> = ({ onFormSubmit, onCancel, ...props }) => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<Institution>();

  return (
    <form className="form">
      <div className="input-container">
        <div className="input-wrapper">
          <label className="label">Nom de l'etablissement</label>
          <input
            {...register("attributes.nom", { required: true, maxLength: 20 })}
            className="input"
          />
          <p className="error">
            {errors.attributes?.nom != null && "Nom is required"}
          </p>
        </div>
      </div>

      <div className="button-container">
        <Button
          message="Ajouter"
          onClick={handleSubmit(onFormSubmit)}
          className="valid"
        />

        <Button message="Revenir" onClick={onCancel} className="invalid" />
      </div>
    </form>
  );
};

export default FormAddInstitution;
