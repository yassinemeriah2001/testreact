import { type ComponentProps, type FC } from "react";
import { useForm } from "react-hook-form";
import { Login } from "../Services/Interfaces_Uris_Token";
import Button from "./CustomButton";

export type Form = ComponentProps<"div"> & {
  connect: (values: Login) => any;
};

const Loginform: FC<Form> = ({ connect, ...props }) => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<Login>();

  return (
    <form className="form border-b border-t border-black my-8 ">
      <div className=" justify-center flex my-4">
        <br />
        <div className="input-container">
          <div>
            <h1 className=" justify-center mx-56 flex font-extrabold">Login</h1>
            <br />
          </div>
          <div className="input-wrapper">
            <label className="label">Identifiant</label>
            <input
              placeholder="Identifiant"
              {...register("identifier", { required: true, maxLength: 40 })}
              className="input bg-gray-50"
            />
            <p className="error">
              {errors.identifier != null && "identfiant is required"}
            </p>
          </div>
          <div className="input-wrapper">
            <label className="label">Password</label>
            <input
              placeholder="Password"
              type="Password"
              {...register("password", { required: true, maxLength: 20 })}
              className="input bg-gray-50"
            />
            <p className="error">
              {errors.identifier != null && "password is required"}
            </p>
          </div>
        </div>
      </div>

      <div className="button-container justify-center flex">
        <Button
          className="valid bg-blue-200 mx-4"
          message="Connecter"
          onClick={handleSubmit(connect)}
        />
        <button className="invalid border bg-red-200 mx-4" type="reset">
          Reintaliser
        </button>
      </div>
      <br />
    </form>
  );
};

export default Loginform;
