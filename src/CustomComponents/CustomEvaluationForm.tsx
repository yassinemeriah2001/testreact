import { type ComponentProps, type FC } from "react";
import { useForm } from "react-hook-form";
import { NavLink } from "react-router-dom";
import { Evaluation } from "../Services/Interfaces_Uris_Token";
import Button from "./CustomButton";

export type Eval = ComponentProps<"div"> & {
  add: (values: Evaluation) => any;
};

const Rate: FC<Eval> = ({ add, ...props }) => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<Evaluation>();

  return (
    <form className="form">
      <div className="input-container">
        <div className="input-wrapper">
          <label className="label">Theme Formation</label>
          <input
            {...register("Theme", { required: true, maxLength: 20 })}
            className="input"
          />
          <p className="error">{errors.Theme != null && "Theme is required"}</p>
          <label className="label">Date Formation</label>
          <input
            type="date"
            {...register("Date", { required: true, maxLength: 20 })}
            className="input"
          />
          <p className="error">{errors.Date != null && "Date is required"}</p>
          <label className="label">Formateur</label>
          <input
            {...register("Formateur", { required: true, maxLength: 20 })}
            className="input"
          />
          <p className="error">
            {errors.Formateur != null && "Instructor is required"}
          </p>
        </div>
        <div className="input-wrapper">
          <label className="label">Nom</label>
          <input
            {...register("Nom", { required: true, maxLength: 20 })}
            className="input"
          />
          <p className="error">{errors.Nom != null && "Nom is required"}</p>
          <label className="label">Prenom</label>
          <input
            {...register("Prenom", { required: true, maxLength: 20 })}
            className="input"
          />
          <p className="error">
            {errors.Prenom != null && "Prenom is required"}
          </p>
          <label className="label">Telephone</label>
          <input
            type="number"
            min="10000000"
            {...register("Telephone", { required: true, maxLength: 20 })}
            className="input"
          />
          <p className="error">
            {errors.Telephone != null && "Telephone is required"}
          </p>
        </div>
        <div className="input-wrapper">
          <label className="label">Email</label>
          <input
            {...register("Email", {
              required: true,
              pattern: {
                value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                message: "Enter a valid e-mail address",
              },
            })}
            className="input"
          />
          <p className="error">{errors.Email != null && "Email is required"}</p>
          <br />
          <label className="label">Etablissement</label>
          <input
            type="text"
            {...register("Etablissement", { required: true, maxLength: 20 })}
            className="input"
          />
          <p className="error">
            {errors.Etablissement != null && "Etablissement is required"}
          </p>
        </div>
        <div className="input-wrapper">
          <label className="label">Evaluation</label>
          <input
            type="number"
            min="0"
            max="11"
            {...register("Eval", { required: true, maxLength: 20 })}
            className="input"
          />
          <p className="error">{errors.Eval != null && "Eval is required"}</p>
        </div>
      </div>

      <div className="button-container">
        <Button
          className="valid"
          message="Ajouter"
          onClick={handleSubmit(add)}
        />
        <button className="invalid" type="reset">
          Reintaliser
        </button>
        <NavLink to={"/login"} className="justify-end flex">
          Admin?Login
        </NavLink>
      </div>
    </form>
  );
};

export default Rate;
