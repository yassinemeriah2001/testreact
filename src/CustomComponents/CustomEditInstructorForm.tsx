import { type ComponentProps, type FC } from "react";
import { useForm } from "react-hook-form";
import { NavLink } from "react-router-dom";
import { type Instructor } from "../Services/Interfaces_Uris_Token";
import Button from "./CustomButton";

export type Form = ComponentProps<"div"> & {
  onUpdate: (i: number, values: Instructor) => any;
  i: number;
  instructor: Instructor | undefined;
};

const FormEditInstructor: FC<Form> = ({
  onUpdate,
  i,
  instructor,
  ...props
}) => {
  const {
    register,
    handleSubmit,
    formState: { errors },
    getValues,
  } = useForm<Instructor>();
  const values = getValues();

  return (
    <form className=" form ">
      <div className="input-container">
        <label className="label">Id</label>
        <input className="input" value={i} readOnly disabled />

        <div className="input-wrapper">
          <label className="label">Nom du instructor</label>
          <input
            {...register("attributes.Nom", { required: true, maxLength: 20 })}
            className="input"
            defaultValue={instructor?.attributes.Nom}
          />
          <p className=" error">
            {errors.attributes?.Nom != null && "Nom is required"}
          </p>
        </div>
        <div className="input-wrapper">
          <label className="label">Prenom du instructor</label>
          <input
            {...register("attributes.prenom", { required: true })}
            className="input"
            defaultValue={instructor?.attributes.prenom}
          />
          <p className=" error">
            {" "}
            {errors.attributes?.prenom != null && "Prenom is required"}
          </p>
        </div>
        <div className="input-wrapper">
          <label className="label">Sexe</label>
          <select
            {...register("attributes.sexe", { required: true })}
            className="block appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-4 py-2 pr-8 rounded shadow leading-tight focus:outline-none focus:shadow-outline
    i"
            defaultValue={instructor?.attributes.sexe}
          >
            <option value="Male">Male</option>
            <option value="Female">Female</option>
          </select>
          <p className=" error">
            {" "}
            {errors.attributes?.sexe != null && "Sexe is required"}
          </p>
        </div>
        <div className="input-wrapper">
          <label className="label">Telephone du instructor</label>
          <input
            type="number"
            min="10000000"
            {...register("attributes.Tel", {
              required: true,
              maxLength: { value: 8, message: "must be 8 numbers" },
              minLength: { value: 8, message: "must be 8 numbers" },
            })}
            className="input"
            defaultValue={instructor?.attributes.Tel}
          />
          <p className=" error">
            {errors.attributes?.Tel?.message &&
              "Telephone is required" &&
              errors.attributes.Tel?.message}
          </p>
        </div>
        <div className="input-wrapper">
          <label className="label">Email du instructor</label>
          <input
            {...register("attributes.email", {
              required: true,
              pattern: {
                value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                message: "Enter a valid e-mail address",
              },
            })}
            className="input"
            defaultValue={instructor?.attributes.email}
          />
          <p className=" error">{errors.attributes?.email?.message}</p>
        </div>
        <div className="button-container">
          <Button
            className="valid"
            message="Modifier"
            onClick={handleSubmit(() => onUpdate(i, values))}
          />
          <NavLink to={"/admin/formateurs"}>
            <Button className="invalid" message="Revenir" />
          </NavLink>
        </div>
      </div>
    </form>
  );
};

export default FormEditInstructor;
