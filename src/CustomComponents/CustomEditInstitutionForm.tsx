import { type ComponentProps, type FC } from "react";
import { useForm } from "react-hook-form";
import { NavLink } from "react-router-dom";
import { type Institution } from "../Services/Interfaces_Uris_Token";
import Button from "./CustomButton";

export type Form = ComponentProps<"div"> & {
  onUpdate: (i: number, values: Institution) => any;
  i: number;
  institution: Institution | undefined;
};

const FormEditInstitution: FC<Form> = ({
  onUpdate,
  i,
  institution,
  ...props
}) => {
  const {
    register,
    handleSubmit,
    formState: { errors },
    getValues,
  } = useForm<Institution>();
  const values = getValues();

  return (
    <form className="form">
      <div className="input-container">
        <div className="input-wrapper">
          <label className="label">Id</label>
          <input className="input" value={i} readOnly disabled />
          <label className="label">Nom de l'etablissement</label>
          <input
            {...register("attributes.nom", { required: true, maxLength: 20 })}
            className="input"
            defaultValue={institution?.attributes.nom}
          />
          <p className="error">
            {errors.attributes?.nom != null && "Nom is required"}
          </p>
        </div>
      </div>

      <div className="button-container">
        <Button
          className="valid"
          message="Modifier"
          onClick={handleSubmit(() => onUpdate(i, values))}
        />
        <NavLink to={"/admin/etablissements"}>
          <Button className="invalid" message="Revenir" />
        </NavLink>
      </div>
    </form>
  );
};

export default FormEditInstitution;
