import { type ComponentProps, type FC } from "react";
export type TtileProps = ComponentProps<"h1"> & {
  title: string;
  undertitle: string;
};

const Title: FC<TtileProps> = ({ title, undertitle, ...props }) => {
  return (
    <h1 className={props.className}>
      {title}
      <span className="title-span">{undertitle}</span>
    </h1>
  );
};

export default Title;
