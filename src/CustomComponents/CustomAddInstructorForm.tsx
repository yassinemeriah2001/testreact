import { type ComponentProps, type FC } from "react";
import { useForm } from "react-hook-form";
import { type Instructor } from "../Services/Interfaces_Uris_Token";
import Button from "./CustomButton";

export type Form = ComponentProps<"form"> & {
  onFormSubmit: (values: Instructor) => any;
  onCancel: () => void;
};

const FormAddInstructor: FC<Form> = ({ onFormSubmit, onCancel, ...props }) => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<Instructor>();

  return (
    <form className=" form ">
      <div className="input-container">
        <label className="label">Nom du formateur</label>
        <input
          {...register("attributes.Nom", { required: true, maxLength: 20 })}
          className="input"
        />
        <p className=" error">
          {errors.attributes?.Nom != null && "Nom is required"}
        </p>

        <label className="label">Prenom du formateur</label>
        <input
          {...register("attributes.prenom", { required: true })}
          className="input"
        />
        <p className=" error">
          {" "}
          {errors.attributes?.prenom != null && "Prenom is required"}
        </p>
      </div>
      <div className="input-wrapper">
        <label className="label">Sexe</label>
        <select
          {...register("attributes.sexe", { required: true })}
          className="block appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-4 py-2 pr-8 rounded shadow leading-tight focus:outline-none focus:shadow-outline
    i"
        >
          <option value="Male">Male</option>
          <option value="Female">Female</option>
        </select>
        <p className=" error">
          {" "}
          {errors.attributes?.sexe != null && "Sexe is required"}
        </p>
      </div>
      <div className="input-wrapper">
        <label className="label">Telephone du formateur</label>
        <input
          type="number"
          min="10000000"
          {...register("attributes.Tel", {
            required: true,
            maxLength: { value: 8, message: "must be 8 numbers" },
            minLength: { value: 8, message: "must be 8 numbers" },
          })}
          className="input"
        />
        <p className=" error">
          {errors.attributes?.Tel?.message &&
            "Telephone is required" &&
            errors.attributes.Tel?.message}
        </p>
      </div>
      <div className="input-wrapper">
        <label className="label">Email du formateur</label>
        <input
          {...register("attributes.email", {
            required: true,
            pattern: {
              value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
              message: "Enter a valid e-mail address",
            },
          })}
          className="input"
        />
        <p className=" error">{errors.attributes?.email?.message}</p>
      </div>
      <div className="button-container">
        <Button
          className="valid"
          message="Ajouter"
          onClick={handleSubmit(onFormSubmit)}
        />

        <Button className="invalid" message="Revenir" onClick={onCancel} />
      </div>
    </form>
  );
};

export default FormAddInstructor;
