import { type ComponentProps, type FC } from "react";
import { useForm } from "react-hook-form";
import { type Training } from "../Services/Interfaces_Uris_Token";
import Button from "./CustomButton";

export type Form = ComponentProps<"form"> & {
  onFormSubmit: (values: Training) => any;
  onCancel: () => void;
};

const FormAddTraining: FC<Form> = ({ onFormSubmit, onCancel, ...props }) => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<Training>();

  return (
    <form className=" form ">
      <div className="input-container">
        <div className="input-wrapper">
          <label className="label">Theme de la formation</label>
          <input
            {...register("attributes.Theme", { required: true, maxLength: 20 })}
            className="input"
          />
          <p className="error">
            {errors.attributes?.Theme != null && "Theme is required"}
          </p>
        </div>
      </div>

      <div className="button-container">
        <Button
          className="valid"
          message="Ajouter"
          onClick={handleSubmit(onFormSubmit)}
        />

        <Button className="invalid" message="Revenir" onClick={onCancel} />
      </div>
    </form>
  );
};

export default FormAddTraining;
