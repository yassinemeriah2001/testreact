import { type ComponentProps, type FC } from "react";
import { NavLink } from "react-router-dom";
import { type Instructor } from "../Services/Interfaces_Uris_Token";

export type TableList = ComponentProps<"table"> & {
  arrayInstructor: Instructor[];
  onDelete: (i: number) => Promise<void>;
};

const InstructorTable: FC<TableList> = ({
  arrayInstructor,
  onDelete,
  ...props
}) => {
  return (
    <table className="table ">
      <thead className="thead">
        <tr>
          <th className="th">Nom</th>
          <th className="th">Prenom</th>
          <th className="th">Sexe</th>
          <th className="th">Tel</th>
          <th className="th">Email</th>
          <th className="th">CreatedAt</th>
          <th className="th">ModifiedAt</th>
          <th className="th">Modifier</th>
          <th className="th">Supprimer</th>
        </tr>
      </thead>
      <tbody className="tbody">
        {arrayInstructor?.map((arrayInstructor) => {
          const creationDate = new Date(
            arrayInstructor.attributes.createdAt,
          ).toLocaleString();
          const upDate = new Date(
            arrayInstructor.attributes.updatedAt,
          ).toLocaleString();
          return (
            <tr key={arrayInstructor.id} className="tr">
              <td className="td capitalize">
                {arrayInstructor.attributes.Nom}
              </td>
              <td className="td capitalize">
                {arrayInstructor.attributes.prenom}
              </td>
              <td className="td capitalize">
                {arrayInstructor.attributes.sexe}
              </td>
              <td className="td">{arrayInstructor.attributes.Tel}</td>
              <td className="td">{arrayInstructor.attributes.email}</td>
              <td className="td">{creationDate}</td>
              <td className="td">{upDate}</td>
              <td className="td">
                <NavLink to={"/admin/formateurs/:" + arrayInstructor.id}>
                  {" "}
                  <button className="modif">modifier</button>
                </NavLink>
              </td>
              <td className="td">
                <button
                  className="supp"
                  onClick={async () => {
                    await onDelete(arrayInstructor.id);
                  }}
                >
                  supprimer
                </button>
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
};

export default InstructorTable;
