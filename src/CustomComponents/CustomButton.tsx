import { type ComponentProps, type FC } from "react";
export type ButtonProps = ComponentProps<"button"> & {
  message: string;
};

const Button: FC<ButtonProps> = ({ message, ...props }) => {
  return <button {...props}>{message}</button>;
};

export default Button;
