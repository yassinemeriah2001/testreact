import { type ComponentProps, type FC } from "react";
import { useForm } from "react-hook-form";
import { NavLink } from "react-router-dom";
import { type Training } from "../Services/Interfaces_Uris_Token";
import Button from "./CustomButton";

export type Form = ComponentProps<"form"> & {
  onUpdate: (i: number, values: Training) => any;
  i: number;
  training: Training | undefined;
};

const FormEditTraining: FC<Form> = ({ onUpdate, i, training, ...props }) => {
  const {
    register,
    handleSubmit,
    formState: { errors },
    getValues,
  } = useForm<Training>();
  const values = getValues();

  return (
    <form className=" form ">
      <div className="input-container">
        <div className="input-wrapper">
          <label className="label">Id</label>
          <input className="input" value={i} readOnly disabled />
          <label className="label">Theme de la training</label>
          <input
            {...register("attributes.Theme", { required: true, maxLength: 20 })}
            className="input"
            defaultValue={training?.attributes.Theme}
          />
          <p className="error">
            {errors.attributes?.Theme != null && "Theme is required"}
          </p>
        </div>
      </div>

      <div className="button-container">
        <Button
          className="valid"
          message="Modifier"
          onClick={handleSubmit(() => onUpdate(i, values))}
        />
        <NavLink to={"/admin/etablissements"}>
          <Button className="invalid" message="Revenir" />
        </NavLink>
      </div>
    </form>
  );
};

export default FormEditTraining;
