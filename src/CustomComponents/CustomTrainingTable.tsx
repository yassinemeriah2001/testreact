import { type ComponentProps, type FC } from "react";
import { NavLink } from "react-router-dom";
import { type Training } from "../Services/Interfaces_Uris_Token";

export type TableList = ComponentProps<"table"> & {
  arrayTraining: Training[];
  onDelete: (i: number) => Promise<void>;
};

const TrainingTable: FC<TableList> = ({
  arrayTraining,
  onDelete,
  ...props
}) => {
  return (
    <table className="table ">
      <thead className="thead">
        <tr>
          <th className="th">Nom</th>
          <th className="th">CreatedAt</th>
          <th className="th">ModifiedAt</th>
          <th className="th">Modifier</th>
          <th className="th">Supprimer</th>
        </tr>
      </thead>
      <tbody className="tbody">
        {arrayTraining?.map((arrayTraining) => {
          const creationDate = new Date(
            arrayTraining.attributes.createdAt,
          ).toLocaleString();

          const upDate = new Date(
            arrayTraining.attributes.updatedAt,
          ).toLocaleString();

          return (
            <tr key={arrayTraining.id} className="tr">
              <td className="td uppercase">{arrayTraining.attributes.Theme}</td>
              <td className="td">{creationDate}</td>
              <td className="td">{upDate}</td>
              <td className="td">
                <NavLink to={"/admin/formations/:" + arrayTraining.id}>
                  {" "}
                  <button className="modif">modifier</button>
                </NavLink>
              </td>
              <td className="td">
                <button
                  className="supp"
                  onClick={async () => {
                    await onDelete(arrayTraining.id);
                  }}
                >
                  supprimer
                </button>
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
};

export default TrainingTable;
