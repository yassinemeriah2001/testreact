import { type ComponentProps, type FC } from "react";
import { NavLink } from "react-router-dom";
import { type Institution } from "../Services/Interfaces_Uris_Token";

export type TableList = ComponentProps<"table"> & {
  arrayInstitution: Institution[];
  onDelete: (i: number) => Promise<void>;
};

const InstitutionTable: FC<TableList> = ({
  arrayInstitution,
  onDelete,
  ...props
}) => {
  return (
    <table className="table ">
      <thead className="thead">
        <tr>
          <th className="th">Nom</th>
          <th className="th">CreatedAt</th>
          <th className="th">ModifiedAt</th>
          <th className="th">Modifier</th>
          <th className="th">Supprimer</th>
        </tr>
      </thead>
      <tbody className="tbody">
        {arrayInstitution?.map((arrayInstitution) => {
          const creationDate = new Date(
            arrayInstitution.attributes.createdAt,
          ).toLocaleString();

          const upDate = new Date(
            arrayInstitution.attributes.updatedAt,
          ).toLocaleString();

          return (
            <tr key={arrayInstitution.id} className="tr">
              <td className="td uppercase">
                {arrayInstitution.attributes.nom}
              </td>
              <td className="td">{creationDate}</td>
              <td className="td">{upDate}</td>
              <td className="td">
                <NavLink to={"/admin/etablissements/:" + arrayInstitution.id}>
                  {" "}
                  <button className="modif">modifier</button>
                </NavLink>
              </td>
              <td className="td">
                <button
                  className="supp"
                  onClick={async () => {
                    await onDelete(arrayInstitution.id);
                  }}
                >
                  supprimer
                </button>
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
};

export default InstitutionTable;
