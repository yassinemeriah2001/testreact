/*import { useAuth } from "Context/AuthContext";
import { useContext } from "react";
import { Navigate, Route } from "react-router-dom";

const ProtectedRoute: React.FC<{
  component: React.ComponentType<any>;
  path: string;
}> = ({ component: Component, path, ...props }) => {
  const { authenticated } = useAuth();

  return (
    <Route
      path={path}
      render={() =>
        authenticated ? (
          <Component {...props} />
        ) : (
          <Navigate to={{ pathname: "/" }} />
        )
      }
    />
  );
};
export default ProtectedRoute;*/

import { Navigate } from "react-router-dom";
import { useAuth } from "Context/AuthContext";
import { FC, ReactNode } from "react";

export const ProtectedRoute: FC<any> = ({ children }) => {
  const { authenticated } = useAuth();
  if (!authenticated) {
    // user is not authenticated
    return <Navigate to="/" />;
  }
  return children;
};
