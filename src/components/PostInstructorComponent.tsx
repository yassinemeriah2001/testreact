import React from "react";
import { useMutation, useQueryClient } from "react-query";
import Title from "../CustomComponents/CustomTitle";
import FormAddInstructor from "../CustomComponents/CustomAddInstructorForm";
import { addData } from "../Services/Functions";
import {
  type Add,
  type ButtonProp,
  type Instructor,
  UriFormtr,
} from "../Services/Interfaces_Uris_Token";

const FormInstructor: React.FC<ButtonProp> = ({ handleclose }) => {
  const useAdd = () => {
    const queryClient = useQueryClient();
    return useMutation({
      mutationFn: async ({ uri, data }: Add) => await addData(uri, data),
      onSuccess: () => {
        queryClient.invalidateQueries({ queryKey: ["Instructor"] });
      },
    });
  };
  const { mutate: add } = useAdd();
  const onSubmit = (values: Instructor) => {
    add({
      uri: UriFormtr,
      data: JSON.stringify({ data: values.attributes }),
    });
    handleclose();
  };

  return (
    <div className="form-container">
      <div className="div-form">
        <div className="Title-Div">
          <Title
            title={"Formateur"}
            className="title-form"
            undertitle={"AJOUT"}
          />
        </div>
        <div className="form-div">
          <FormAddInstructor onFormSubmit={onSubmit} onCancel={handleclose} />
        </div>
      </div>
    </div>
  );
};

export default FormInstructor;
