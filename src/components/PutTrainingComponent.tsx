import React, { useEffect, useState } from "react";
import { useMutation, useQuery, useQueryClient } from "react-query";
import { useNavigate, useParams } from "react-router-dom";
import Title from "../CustomComponents/CustomTitle";
import FormEditTraining from "../CustomComponents/CustomEditTrainingForm";
import { getDataById, updateData } from "../Services/Functions";
import {
  type ApiResponse,
  type Training,
  UriForm,
  type Update,
} from "../Services/Interfaces_Uris_Token";

const FormTrainingPut = () => {
  const navigate = useNavigate();
  const { id } = useParams();
  const useUpdate = () => {
    const queryClient = useQueryClient();
    return useMutation({
      mutationFn: async ({ uri, data, id }: Update) =>
        await updateData(uri, data, id),
      onSuccess: () => {
        queryClient.invalidateQueries({ queryKey: ["training"] });
      },
    });
  };
  const { mutate: update } = useUpdate();

  const onSubmit = (i: number, values: Training) => {
    update({
      uri: UriForm,
      data: JSON.stringify({ data: values.attributes }),
      id: i,
    });
    navigate("/admin/formations");
  };
  const { data } = useQuery<ApiResponse<Training>>(
    "fetchingOne",
    async () => await getDataById(UriForm, Number(id?.toString().substring(1))),
  );
  const [training, setTraining] = useState<Training | undefined>();
  useEffect(() => {
    if (data?.data != null) {
      setTraining(data?.data);
    }
  }, [data?.data]);

  return (
    <div className="form-container">
      <div className="div-form">
        <div className="Title-Div">
          <Title
            title={"Formation"}
            className="title-form"
            undertitle={"Modif"}
          />
        </div>
        <div className="form-div">
          <FormEditTraining
            onUpdate={onSubmit}
            i={Number(id?.toString().substring(1))}
            training={training}
          />
        </div>
      </div>
    </div>
  );
};

export default FormTrainingPut;
