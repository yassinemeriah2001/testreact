import React from "react";
import { useMutation, useQueryClient } from "react-query";
import Title from "../CustomComponents/CustomTitle";
import FormAddInstitution from "../CustomComponents/CustomAddInstitutionForm";
import { addData } from "../Services/Functions";
import {
  type ButtonProp,
  type Institution,
  UriEtab,
  type Add,
} from "../Services/Interfaces_Uris_Token";

const FormInstitution: React.FC<ButtonProp> = ({ handleclose }) => {
  const useAdd = () => {
    const queryClient = useQueryClient();
    return useMutation({
      mutationFn: async ({ uri, data }: Add) => await addData(uri, data),
      onSuccess: () => {
        queryClient.invalidateQueries({ queryKey: ["Institution"] });
      },
    });
  };
  const { mutate: add } = useAdd();
  const onSubmit = (values: Institution) => {
    add({
      uri: UriEtab,
      data: JSON.stringify({ data: values.attributes }),
    });
    handleclose();
  };

  return (
    <div className="form-container">
      <div className="div-form">
        <div className="Title-Div">
          <Title
            title={"Etablissement"}
            className="title-form"
            undertitle={"AJOUT"}
          />
        </div>
        <div className="form-div">
          <FormAddInstitution onFormSubmit={onSubmit} onCancel={handleclose} />
        </div>
      </div>
    </div>
  );
};

export default FormInstitution;
