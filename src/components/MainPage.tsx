import React from "react";
import { Outlet } from "react-router-dom";
import Sidebar from "./SideBar";

const MainPage = () => {
  return (
    <React.Fragment>
      {/* heading section */}
      <section>
        <div></div>
      </section>

      {/* sidebar section */}
      <section>
        <div className="grid grid-cols-12">
          <div className="col-span-3  bg-blue-900 h-screen  pl-2 md:col-span-2">
            <Sidebar />
          </div>
          <div className="col-span-9 bg-white h-screen pl-2 md:col-span-10">
            <Outlet />
          </div>
        </div>
      </section>
    </React.Fragment>
  );
};

export default MainPage;
