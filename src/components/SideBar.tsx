import React from "react";
import { RiDashboard2Fill } from "react-icons/ri";
import { NavLink } from "react-router-dom";

const Sidebar = () => {
  return (
    <aside className=" w-70 " aria-label="Sidebar">
      <div className="px-3 py-4 overflow-y-auto rounded  ">
        <RiDashboard2Fill className="text-white text-4xl float-left" />

        <h1 className=" text-white text-3xl font-extrabold text-center">
          Dashboard
        </h1>
        <ul className="   rounded-lg  ">
          <NavLink
            to="/admin/formateurs"
            className="link active:bg-teal-100 font-extrabold  "
          >
            <li>Formateurs</li>
          </NavLink>
          <NavLink
            to="/admin/formations"
            className="link active:bg-teal-100  font-extrabold "
          >
            <li>Formations</li>
          </NavLink>
          <NavLink
            to="/admin/etablissements"
            className="link active:bg-teal-100 font-extrabold "
          >
            <li>Etablissements</li>
          </NavLink>
        </ul>
      </div>
    </aside>
  );
};

export default Sidebar;
