import React from "react";
import { GiBookshelf } from "react-icons/gi";
import { logOut } from "Services/Functions";
import { RiLogoutCircleRLine } from "react-icons/ri";
import { useAuth } from "Context/AuthContext";
import { useNavigate } from "react-router-dom";

const Navbar = () => {
  const { access, onSetAccess, onSetAuthenticated } = useAuth();
  const navigate = useNavigate();
  const submit = () => {
    onSetAccess(null);
    if (access == null) {
      onSetAuthenticated(false);
      navigate("/");
    }
  };
  return (
    <React.Fragment>
      <section>
        <div className="bg-black h-20 w-full flex items-center justify-between pl-10 space-x-4">
          <div className="flex space-x-4">
            <GiBookshelf className="text-white text-4xl" />

            <p className="text-white text-3xl font-extrabold">
              Gestion des formations
            </p>
          </div>
          <div className="flex space-x-6 ">
            <button
              onClick={submit}
              className="text-white text-3xl font-extrabold flex justify-end focus:outline-none  "
            >
              <RiLogoutCircleRLine className="text-white text-4xl mx-4" />
            </button>
          </div>
        </div>
      </section>
    </React.Fragment>
  );
};

export default Navbar;
