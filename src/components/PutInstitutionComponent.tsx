import React, { useEffect, useState } from "react";
import { useMutation, useQuery, useQueryClient } from "react-query";
import { useNavigate, useParams } from "react-router-dom";
import { getDataById, updateData } from "../Services/Functions";
import {
  type ApiResponse,
  type Institution,
  type Update,
  UriEtab,
} from "../Services/Interfaces_Uris_Token";

import FormEditInstitution from "../CustomComponents/CustomEditInstitutionForm";
import Title from "../CustomComponents/CustomTitle";

const FormInstitutionPut = () => {
  const navigate = useNavigate();
  const { id } = useParams();
  const useUpdate = () => {
    const queryClient = useQueryClient();
    return useMutation({
      mutationFn: async ({ uri, data, id }: Update) =>
        await updateData(uri, data, id),
      onSuccess: () => {
        queryClient.invalidateQueries({ queryKey: ["Institution"] });
      },
    });
  };
  const { mutate: update } = useUpdate();

  const onSubmit = (i: number, values: Institution) => {
    update({
      uri: UriEtab,
      data: JSON.stringify({ data: values.attributes }),
      id: i,
    });
    navigate("/admin/etablissements");
  };
  const { data } = useQuery<ApiResponse<Institution>>(
    "fetchingOne",
    async () => await getDataById(UriEtab, Number(id?.toString().substring(1))),
  );
  const [institution, setInstitution] = useState<Institution | undefined>();
  useEffect(() => {
    if (data?.data != null) {
      setInstitution(data?.data);
    }
  }, [data?.data]);

  return (
    <div className="form-container">
      <div
        className="div-form"
        style={{ width: "70%", marginTop: "calc(100vh - 85vh - 20px)" }}
      >
        <div className="Title-Div">
          <Title
            title={"Etablissement"}
            className="title-form"
            undertitle={"Modif"}
          />
        </div>
        <div className="form-div">
          <FormEditInstitution
            onUpdate={onSubmit}
            i={Number(id?.toString().substring(1))}
            institution={institution}
          />
        </div>
      </div>
    </div>
  );
};

export default FormInstitutionPut;
