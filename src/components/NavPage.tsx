import React from "react";
import { Route } from "react-router-dom";

import Error from "../Pages/ErrorPage";
import Loading from "../Pages/LoadingPage";
import FormInstitutionPut from "./PutInstitutionComponent";
import FormInstructorPut from "./PutInstructorComponent";
import FormTrainingPut from "./PutTrainingComponent";
import Institutions from "../Pages/InstitutionsPage";
import Instructors from "../Pages/InstructorsPage";
import Trainings from "../Pages/TrainingsPage";

const NavPage = () => {
  return (
    <>
      <Route path="/admin/etablissements" element={<Institutions />} />
      <Route path="/admin/formateurs" element={<Instructors />} />
      <Route path="/admin/formations" element={<Trainings />} />
      <Route path="/admin/loading" element={<Loading />} />
      <Route path="/admin/error" element={<Error />} />
      <Route
        path="/admin/etablissements/:id"
        element={<FormInstitutionPut />}
      />
      <Route path="/admin/formateurs/:id" element={<FormInstructorPut />} />
      <Route path="/admin/formations/:id" element={<FormTrainingPut />} />
    </>
  );
};

export default NavPage;
