import React, { useEffect, useState } from "react";
import { useMutation, useQuery, useQueryClient } from "react-query";
import { useNavigate, useParams } from "react-router-dom";
import Title from "../CustomComponents/CustomTitle";
import FormEditInstructor from "../CustomComponents/CustomEditInstructorForm";
import { getDataById, updateData } from "../Services/Functions";
import {
  type ApiResponse,
  type Instructor,
  UriFormtr,
  type Update,
} from "../Services/Interfaces_Uris_Token";

const FormInstructorPut = () => {
  const navigate = useNavigate();
  const { id } = useParams();
  const useUpdate = () => {
    const queryClient = useQueryClient();
    return useMutation({
      mutationFn: async ({ uri, data, id }: Update) =>
        await updateData(uri, data, id),
      onSuccess: () => {
        queryClient.invalidateQueries({ queryKey: ["Instructor"] });
      },
    });
  };
  const { mutate: update } = useUpdate();

  const onSubmit = (i: number, values: Instructor) => {
    update({
      uri: UriFormtr,
      data: JSON.stringify({ data: values.attributes }),
      id: i,
    });
    navigate("/admin/formateurs");
  };
  const { data } = useQuery<ApiResponse<Instructor>>(
    "fetchingOne",
    async () =>
      await getDataById(UriFormtr, Number(id?.toString().substring(1))),
  );
  const [instructor, setInstructor] = useState<Instructor | undefined>();
  useEffect(() => {
    if (data?.data != null) {
      setInstructor(data?.data);
    }
  }, [data?.data]);

  return (
    <div className="form-container">
      <div className="div-form">
        <div className="Title-Div">
          <Title
            title={"Formateur"}
            className="title-form"
            undertitle={"Modif"}
          />
        </div>
        <div className="form-div">
          <FormEditInstructor
            onUpdate={onSubmit}
            i={Number(id?.toString().substring(1))}
            instructor={instructor}
          />
        </div>
      </div>
    </div>
  );
};

export default FormInstructorPut;
