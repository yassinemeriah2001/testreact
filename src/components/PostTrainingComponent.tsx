import React from "react";
import { useMutation, useQueryClient } from "react-query";
import Title from "../CustomComponents/CustomTitle";

import FormAddTraining from "../CustomComponents/CustomAddTrainingForm";
import { addData } from "../Services/Functions";

import {
  type Add,
  type ButtonProp,
  type Training,
  UriForm,
} from "../Services/Interfaces_Uris_Token";

const FormTraining: React.FC<ButtonProp> = ({ handleclose }) => {
  const useAdd = () => {
    const queryClient = useQueryClient();
    return useMutation({
      mutationFn: async ({ uri, data }: Add) => await addData(uri, data),
      onSuccess: () => {
        queryClient.invalidateQueries({ queryKey: ["formation"] });
      },
    });
  };
  const { mutate: add } = useAdd();

  const onSubmit = (values: Training) => {
    add({
      uri: UriForm,
      data: JSON.stringify({ data: values.attributes }),
    });
    handleclose();
  };

  return (
    <div className="form-container">
      <div className="div-form">
        <div className="Title-Div">
          <Title
            title={"Formation"}
            className="title-form"
            undertitle={"AJOUT"}
          />
        </div>
        <div className="form-div">
          <FormAddTraining onFormSubmit={onSubmit} onCancel={handleclose} />
        </div>
      </div>
    </div>
  );
};

export default FormTraining;
