import { type } from "@testing-library/user-event/dist/type";
import { createContext, useState, ReactNode, useContext, FC } from "react";

interface Props {
  children?: ReactNode;
}
type Val = string | null;
const AuthContext = createContext<{
  access: Val;
  onSetAccess: (value: Val) => void;
  authenticated: boolean;
  onSetAuthenticated: (value: boolean) => void;
}>({
  access: null,
  onSetAccess: () => null,
  authenticated: false,
  onSetAuthenticated: () => false,
});

export const AuthProvider: FC<Props> = ({ children }) => {
  const [access, setAccess] = useState<string | null>(null);
  const onSetAccess = (value: Val) => {
    setAccess(value);
  };
  const [authenticated, setAuthenticated] = useState(false);
  const onSetAuthenticated = (value: boolean) => {
    setAuthenticated(value);
  };
  return (
    <AuthContext.Provider
      value={{ access, onSetAccess, authenticated, onSetAuthenticated }}
    >
      {children}
    </AuthContext.Provider>
  );
};

export const useAuth = () => {
  return useContext(AuthContext);
};
